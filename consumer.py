from kombu import Connection, Exchange, Queue, Consumer
import requests
import adal

tenantID = "eff7f985-dc58-4935-a906-050609be85c3"
grant_type = "client_credentials"
client_id = "f2642234-880a-465f-bf1a-7fc31130f2fd"
client_secret = "Ld++kH96y5SHXmiGUkdTex44MvOokxrHfXsRemfbL8c="
subscription_id = "64caccf3-b508-41e7-92ed-d7ed95b32621"
resource = "https://management.azure.com/"

rabbit_url = "amqp://172.27.2.167:5672"

conn = Connection(rabbit_url)

exchange = Exchange("example-exchange",type="direct")

queue = Queue(name="example-queue", exchange=exchange, routing_key="BOB")

def vmlist():
  auth_endpoint = "https://login.microsoftonline.com/" + tenantID
  context = adal.AuthenticationContext(auth_endpoint, api_version=None)
  r = context.acquire_token_with_client_credentials(resource, client_id, client_secret)
  token = r['accessToken']

  vm_url = "https://management.azure.com/subscriptions/"+subscription_id+"/providers/Microsoft.Compute/virtualMachines?api-version=2017-12-01"
  vm_header = {
    "Authorization": "Bearer " + token,
  }
  vm_r = requests.get(url=vm_url, headers=vm_header)
  print(vm_r.text)

def storagelist():
  auth_endpoint = "https://login.microsoftonline.com/" + tenantID
  context = adal.AuthenticationContext(auth_endpoint, api_version=None)
  r = context.acquire_token_with_client_credentials(resource, client_id, client_secret)
  token = r['accessToken']

  storage_url = "https://management.azure.com/subscriptions/"+subscription_id+ "/providers/Microsoft.Storage/storageAccounts?api-version=2017-06-01"
  storage_header = {
    "Authorization": "Bearer "+ token,
  }
  storage_r = requests.get(url=storage_url, headers= storage_header)
  print(storage_r.text)

def process_message(body, message):
  x = format(body)
  message.ack()
  if x=="1":
    vmlist()
  if x=="2":
    storagelist()


with Consumer(conn, queues=queue, callbacks=[process_message], accept=["text/plain"]):
  conn.drain_events(timeout=10)
