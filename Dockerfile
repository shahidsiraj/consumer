FROM python:3.6

WORKDIR /app

ADD requirements.txt /app

ADD consumer.py /app

RUN pip3 install -r requirements.txt

CMD ["python","./consumer.py"]
